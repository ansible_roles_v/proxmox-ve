Proxmox Virtual Environment
===========================
Installs and configures Proxmox Virtual Environment.  
Proxmox Virtual Environment is an open source server virtualization management solution based on QEMU/KVM and LXC.  

Include role
------------
```yaml
- name: proxmox_ve  
  src: https://gitlab.com/ansible_roles_v/proxmox-ve/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - proxmox_ve
```